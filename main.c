#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include "vvw.h"

#define HEADER "header.txt"

int main(int argc, char const *argv[])
{
  srand(time(NULL)); // seed rand() with current time
  display_header(HEADER);
  while (process_input()) { }
  return 0;
}

void display_header(char *header_file)
{
  FILE *fp;
  char line[80];

  fp = fopen(header_file, "rt");
  if (!fp) die("Failed to open header.");

  while (!feof(fp)) {
    if (fgets(line, 80, fp)) {
      printf("%s", line);
    }
  }
  fclose(fp);
}

void die(const char *message)
{
  if (errno) {
    perror(message);
  } else {
    printf("ERROR: %s\n", message);
  }

  exit(1);
}

int process_input()
{
  printf("Enter the number of battles: ");

  int n;
  scanf("%d", &n);
  struct Unit *vampire = create_random_unit(VAMPIRE);
  struct Unit *werewolf = create_random_unit(WEREWOLF);

  printf("Vampire atk: %d\n", vampire->atk);
  printf("Werewolf atk: %d\n", werewolf->atk);

  return 0;
}

struct Unit *create_random_unit(Race race)
{
  int *randomStats = create_random_atk_def();
  return Unit_create(randomStats[0], randomStats[1], race);
}

int *create_random_atk_def()
{
  int stats[2] = { rand() % 50 + 50, rand() % 50 + 50 };
  return stats;
}

struct Unit *Unit_create(int atk, int def, Race race)
{
  struct Unit *unit = malloc(sizeof(struct Unit));
  if (!unit) die("Memory error");

  unit->atk = atk;
  unit->def = def;
  unit->race = race;

  return unit;
}

void Unit_destroy(struct Unit *self)
{
  free(self);
}
