#ifndef _vvw_h
#define _vvw_h

typedef enum {
  VAMPIRE, WEREWOLF
} Race;

struct Unit {
  int atk;
  int def;
  Race race;
};

int Unit_init(struct Unit *self);
void Unit_destroy(struct Unit *self);
struct Unit *Unit_create(int atk, int def, Race race);
void Unit_set(struct Unit *self, int atk, int def);

void display_header(char *header_file);
int process_input();
void die(const char *message);
struct Unit *create_random_unit(Race race);
int *create_random_atk_def();

#endif
