Challenges Encountered:
=======================
1. Reading a file with a header for introducing the program.
    - Tried reading with GNU extensions [`getline()`](http://www.gnu.org/software/libc/manual/html_node/Line-Input.html) function but wouldn't compile on Windows.
    - used fgets instead to read at most 80 characters from each line.
2. Header text used ASCII blocks (like ASCII-219). Wouldn't render in Windows so had to settle for another way of making ASCII art.
